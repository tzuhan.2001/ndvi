import cv2
import numpy as np
import time

from fastiecm import fastiecm

import spidev as SPI
from lib import LCD_2inch4
from PIL import Image,ImageDraw,ImageFont

# Raspberry Pi pin configuration:
RST = 27
DC = 25
BL = 18
bus = 0 
device = 0 
try:
    # display with hardware SPI:
    ''' Warning!!!Don't  creation of multiple displayer objects!!! '''
    #disp = LCD_2inch4.LCD_2inch4(spi=SPI.SpiDev(bus, device),spi_freq=10000000,rst=RST,dc=DC,bl=BL)
    disp = LCD_2inch4.LCD_2inch4()
    # Initialize library.
    disp.Init()
    # Clear display.
    disp.clear()

    # Create blank image for drawing.
    image1 = Image.new("RGB", (disp.width, disp.height ), "WHITE")
    draw = ImageDraw.Draw(image1)

    #logging.info("draw point")

    draw.rectangle((5,10,6,11), fill = "BLACK")
    draw.rectangle((5,25,7,27), fill = "BLACK")
    draw.rectangle((5,40,8,43), fill = "BLACK")
    draw.rectangle((5,55,9,59), fill = "BLACK")

    #logging.info("draw line")
    draw.line([(20, 10),(70, 60)], fill = "RED",width = 1)
    draw.line([(70, 10),(20, 60)], fill = "RED",width = 1)
    draw.line([(170,15),(170,55)], fill = "RED",width = 1)
    draw.line([(150,35),(190,35)], fill = "RED",width = 1)

    #logging.info("draw rectangle")
    draw.rectangle([(20,10),(70,60)],fill = "WHITE",outline="BLUE")
    draw.rectangle([(85,10),(130,60)],fill = "BLUE")

    #logging.info("draw circle")
    draw.arc((150,15,190,55),0, 360, fill =(0,255,0))
    draw.ellipse((150,65,190,105), fill = (0,255,0))

    image1=image1.rotate(0)
    disp.ShowImage(image1)
    time.sleep(1)
except IOError as e:
    exit()    
except KeyboardInterrupt:
    disp.module_exit()
    exit()

def cap_init():
    cap = cv2.VideoCapture(0)
    ori_w = cap.get(cv2.CAP_PROP_FRAME_WIDTH) 
    ori_h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT) 
    # print (ori_w,ori_h)
    #use Noir camera
    # width = 640*2
    # height = 480*2
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    # cap.set(cv2.CAP_PROP_FPS, 1)
    return cap

def display(image, image_name):
    image = np.array(image, dtype=float)/float(255)
    #shape = image.shape
    #height = int(shape[0] / 2)
    #width = int(shape[1] / 2)
    #image = cv2.resize(image, (width, height))
    cv2.imshow("foo", image)

def contrast_stretch(im):
    in_min = np.percentile(im, 5)
    in_max = np.percentile(im, 95)

    out_min = 0.0
    out_max = 255.0

    out = im - in_min
    out *= ((out_min - out_max) / (in_min - in_max + 0.0001))
    out += in_min
    return out

def calc_ndvi(image):
    b, g, r = cv2.split(image)
    bottom = (r.astype(float) + b.astype(float))
    bottom[bottom==0] = 0.0001
    # ndvi = (b.astype(float) - r) / bottom
    ndvi = (r.astype(float) - b) / bottom #for NoIR
    return ndvi



ndvi_on = False
#cv2.namedWindow("foo", cv2.WINDOW_NORMAL)
#cv2.setWindowProperty("foo", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cap = cap_init()
j = 50
while True:
    i = 5
    while (i > 0):
        i -= 1
        ret, original = cap.read()
    disp_img = original
    if ndvi_on:
        contrasted = contrast_stretch(original)
        ndvi = calc_ndvi(contrasted)
        ndvi_contrasted = contrast_stretch(ndvi)
        disp_img = ndvi_contrasted.astype(np.uint8)
        color_mapped_prep = ndvi_contrasted.astype(np.uint8)
        color_mapped_image = cv2.applyColorMap(color_mapped_prep, fastiecm)
        disp_img = color_mapped_image

    #display(disp_img, 'winshow')
    disp_img = cv2.resize(disp_img,(320,240))
    img = Image.fromarray(cv2.cvtColor(disp_img, cv2.COLOR_BGR2RGB))
    disp.ShowImage(img)
    if (j == 0):
        j = 51
        ndvi_on = ~ndvi_on
    j = j-1
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    if key == ord('s'):
        cv2.imwrite('result.jpg', color_mapped_image)
    if key == ord('m'):
        ndvi_on = ~ndvi_on
        
cv2.destroyAllWindows()
cap.release() 
disp.module_exit()

