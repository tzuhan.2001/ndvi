# NDVI

this project comes from [Capture plant health with NDVI and Raspberry Pi](https://projects.raspberrypi.org/en/projects/astropi-ndvi)

other refs:
1. https://chtseng.wordpress.com/2017/04/24/ndvi%E8%88%87%E6%99%BA%E6%85%A7%E8%BE%B2%E6%A5%AD/
2. https://tech.swcb.gov.tw/EPaper/Home/EPaper?PaperID=9ba18a16-9961-462f-8f6e-e5ed46700173
3. [LCD lib](https://www.waveshare.net/w/upload/a/a8/LCD_Module_RPI_code.7z)